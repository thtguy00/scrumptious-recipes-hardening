from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse_lazy
from Mealplans.models import Mealplans
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.edit import UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
class MealplansListView(LoginRequiredMixin, ListView):
    model = Mealplans
    template_name = "mealplans/list.html"
    paginate_by = 2
    
    def get_queryset(self):
        return Mealplans.objects.filter(owner=self.request.user)

class MealplansDetailView(DetailView):
    model = Mealplans
    template_name = "Mealplans/detail.html"

    def get_queryset(self):
        return Mealplans.objects.filter(owner=self.request.user)

class MealplansCreateView(LoginRequiredMixin, CreateView):
    model = Mealplans
    template_name = "mealplans/new.html"
    fields = ["name", "date", "owner", "recipes"]
    # success_url = reverse_lazy("mealplans_create")

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("mealplans_list")

class MealplansUpdateView(LoginRequiredMixin, UpdateView):
    model = Mealplans
    template_name = "mealplans/edit.html"
    fields = ["name", "date", "owner", "recipes"]
    success_url = reverse_lazy("mealplans_edit")

    def get_queryset(self):
        return Mealplans.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("mealplans_detail", args=[self.object.id])


class MealplansDeleteView(LoginRequiredMixin, DeleteView):
    model = Mealplans
    template_name = "mealplans/delete.html"
    success_url = reverse_lazy("mealplans_list")

    def get_queryset(self):
        return Mealplans.objects.filter(owner=self.request.user)