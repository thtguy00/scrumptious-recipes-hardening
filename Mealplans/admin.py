from django.contrib import admin
from Mealplans.models import Mealplans

# Register your models here.
class MealplanAdmin(admin.ModelAdmin):
    pass

admin.site.register(Mealplans, MealplanAdmin)