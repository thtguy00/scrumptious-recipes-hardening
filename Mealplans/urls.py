from urllib.parse import urlparse
from django.urls import path

from Mealplans.views import MealplansListView
from Mealplans.views import MealplansDetailView
from Mealplans.views import MealplansCreateView
from Mealplans.views import MealplansDeleteView
from Mealplans.views import MealplansUpdateView

urlpatterns = [
    path("", MealplansListView.as_view(), name="mealplans_list"),
    path("<int:pk>/", MealplansDetailView.as_view(), name="mealplans_detail"),
    path("new/", MealplansCreateView.as_view(), name="mealplans_create"),
    path("<int:pk>/delete/", MealplansDeleteView.as_view(), name="mealplans_delete"),
    path("<int:pk>/edit/", MealplansUpdateView.as_view(), name="mealplans_edit"),
]
